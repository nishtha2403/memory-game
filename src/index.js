this.iconsUrl = ["./assets/png/001-dracula.png","./assets/png/002-witch.png","./assets/png/003-jack-o-lantern.png",
    "./assets/png/004-werewolf.png","./assets/png/005-zombie.png","./assets/png/006-frankenstein.png","./assets/png/007-mummy.png",
    "./assets/png/008-pirate.png"];
function generateIcons(n) {
    let pick = n/2;
    let nIcons = this.iconsUrl.slice(0,pick);
    return nIcons;
}

function generateIconPairs(icons) {
    let pairIcons = [];
    icons.forEach((icon) => {
        pairIcons.push(icon);
        pairIcons.push(icon);
    });
    return pairIcons;
}

function shuffleIcons(pairIcons) {
    let shuffledIconsArray = pairIcons.map((icon) => {
        return {key: Math.random(), value: icon}
    })
    .sort((a,b) => {
        return a.key - b.key;
    })
    .map((iconObj) => {
        return iconObj.value;
    });

    return shuffledIconsArray;
}

function preloadIcon(url) {
    let icon = new Image();
    icon.src = url;
}

function preloadIconsArray(iconsArray) {
    iconsArray.forEach((icon) => {
        preloadIcon(icon);
    });
}

function getDashboard() {
    setLocalStorageItems();
    document.getElementById("login-body").style.display = "none";
    document.getElementById("level-wrapper").style.display = "flex";
    document.getElementById("subheading").innerText = "Play";
}

function setLocalStorageItems() {
    fetch("http://localhost:4000/game/current", {
        method:'GET',
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
        }
    })
    .then((resp) => {
        return resp.json();
    })
    .then((data) => {
        if(data !== undefined && data.length > 0) {
            localStorage.setItem("firstlevel1","true");
            localStorage.setItem("firstlevel2","true");
            localStorage.setItem("firstlevel3","true");
            localStorage.setItem("firstlevel4","true");

            data.forEach((game) => {
                if(game.level == 1) {
                    localStorage.setItem("highestScoreLevel1",game['highest_score']);
                    localStorage.setItem("firstlevel1","false");
                    localStorage.setItem("level1ID",game.id);
                } else if(game.level == 2){
                    localStorage.setItem("highestScoreLevel2",game['highest_score']);
                    localStorage.setItem("firstlevel2","false");
                    localStorage.setItem("level2ID",game.id);
                } else if(game.level == 3){
                    localStorage.setItem("highestScoreLevel3",game['highest_score']);
                    localStorage.setItem("firstlevel3","false");
                    localStorage.setItem("level3ID",game.id);
                } else if(game.level == 4){
                    localStorage.setItem("highestScoreLevel4",game['highest_score']);
                    localStorage.setItem("firstlevel4","false");
                    localStorage.setItem("level4ID",game.id);
                }
            });
        } else if( data !== undefined && data.length == 0) {
            localStorage.setItem("firstGame","true");
            localStorage.setItem("firstlevel1","true");
            localStorage.setItem("firstlevel2","true");
            localStorage.setItem("firstlevel3","true");
            localStorage.setItem("firstlevel4","true");
        }
    });
}

function sendGame(level,score,moves) {
    console.log("yes");
    let firstGame = localStorage.getItem("firstGame");
    if(firstGame === "true") {
        console.log("yes");
        fetch('http://localhost:4000/game/new', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
            },
            body: JSON.stringify({
                level,
                score,
                moves,
                highest_score: score
            })
        })
        .then((resp) => {
            return resp.json();
        })
        .then((data) => {
            console.log(data);
        });
    } else {
        let firstlevel1 = localStorage.getItem("firstlevel1");
        let firstlevel2 = localStorage.getItem("firstlevel2");
        let firstlevel3 = localStorage.getItem("firstlevel3");
        let firstlevel4 = localStorage.getItem("firstlevel4");

        let level1ID = localStorage.getItem("level1ID");
        let level2ID = localStorage.getItem("level2ID");
        let level3ID = localStorage.getItem("level3ID");
        let level4ID = localStorage.getItem("level4ID");

        let highestScoreLevel1 = localStorage.getItem("highestScoreLevel1");
        let highestScoreLevel2 = localStorage.getItem("highestScoreLevel2");
        let highestScoreLevel3 = localStorage.getItem("highestScoreLevel3");
        let highestScoreLevel4 = localStorage.getItem("highestScoreLevel4");

        if(firstlevel1 == "true" && level == 1) {
            fetch("http://localhost:4000/game/new", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    level,
                    score,
                    moves,
                    highest_score: score
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        } else if(level == 1) {
            console.log('I am working');
            fetch(`http://localhost:4000/game/${level1ID}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    score,
                    moves,
                    highest_score: highestScoreLevel1
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        }
        if(firstlevel2 == "true" && level == 2) {
            fetch("http://localhost:4000/game/new", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    level,
                    score,
                    moves,
                    highest_score: score
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        } else if(level == 2) {
            fetch(`http://localhost:4000/game/${level2ID}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    score,
                    moves,
                    highest_score: highestScoreLevel2
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        }
        if(firstlevel3 == "true" && level == 3) {
            fetch("http://localhost:4000/game/new", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    level,
                    score,
                    moves,
                    highest_score: score
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        } else if(level == 3) {
            fetch(`localhost:4000/game/${level3ID}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    score,
                    moves,
                    highest_score: highestScoreLevel3
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        }
        if(firstlevel4 == "true" && level == 4) {
            fetch("http://localhost:4000/game/new", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    level,
                    score,
                    moves,
                    highest_score: score
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        } else if(level == 4) {
            fetch(`http://localhost:4000/game/${level4ID}`,{
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("accessToken")
                },
                body: JSON.stringify({
                    score,
                    moves,
                    highest_score: highestScoreLevel4
                })
            }).then((resp) => {
                return resp.json();
            })
            .then((data) => {
                console.log(data);
            });
        }
    }
}

this.refreshInterval;
function game(index) {
    document.getElementsByClassName("game-body")[0].style.display = "flex";
    document.getElementById("hr").innerText = "0";
    document.getElementById("min").innerText = "0";
    document.getElementById("sec").innerText = "0";
    clearInterval(this.refreshInterval);
    let time,startDate = new Date();
    refreshInterval = setInterval(() => {
        let endDate = new Date();
        time = endDate - startDate;
        document.getElementById("hr").innerText = Math.floor(time/(60*60*1000))%24;
        document.getElementById("min").innerText = Math.floor(time/(60*1000))%60;
        document.getElementById("sec").innerText = Math.floor(time/1000)%60;
    }, 1000);

    document.getElementById("brain-logo").addEventListener("click", () => {
        location.reload();
    })

    let icons = generateIcons(index);
    let iconPairs = generateIconPairs(icons);
    let shuffledIconsArray = shuffleIcons(iconPairs);

    let highestScoreLevel1 = localStorage.getItem("highestScoreLevel1");
    let highestScoreLevel2 = localStorage.getItem("highestScoreLevel2");
    let highestScoreLevel3 = localStorage.getItem("highestScoreLevel3");
    let highestScoreLevel4 = localStorage.getItem("highestScoreLevel4");
    if(index === 4 && highestScoreLevel1 !== null) {
        document.getElementById("current-highest").innerText = highestScoreLevel1;
    } else if(index === 8 && highestScoreLevel2 !== null) {
        document.getElementById("current-highest").innerText = highestScoreLevel2;
    } else if(index === 12 && highestScoreLevel3 !== null) {
        document.getElementById("current-highest").innerText = highestScoreLevel3;
    } else if(index === 16 && highestScoreLevel4 !== null) {
        document.getElementById("current-highest").innerText = highestScoreLevel4;
    }

    let lastIcon = "",lastGrid = "",lastGridEvent = "";
    let count = 0, moves = 0, score = 0, matchNumber = 0;

    function listener(event) {
        let currentEvent = event.target;
        let currentGridData = JSON.parse(currentEvent.getAttribute("data-grid"));

        if(currentGridData.isMatched || count === 2) {
            event.preventDefault();
        } else {
            moves += 1;
            count += 1;
            document.getElementById("moves-value").innerText = moves;

            let currentGrid = currentGridData.id;
            let currentIcon = shuffledIconsArray[currentGrid];
    
            currentEvent.style.transform = "rotateY(180deg)";

            setTimeout(() => {
                currentEvent.classList.remove("grid");
                currentEvent.classList.add("grid-visible");
                currentEvent.style.backgroundImage = `url(${currentIcon})`;
                currentEvent.style.pointerEvents = "none";
            }, 300);
            
            if(currentIcon === lastIcon && currentGrid !== lastGrid) {
                lastGridEvent.classList.remove("grid");
                lastGridEvent.classList.add("grid-visible");
                lastGridEvent.style.backgroundImage = `url(${currentIcon})`;
                lastGridEvent.style.pointerEvents = "none";

                let attribute = {
                    id: currentGrid,
                    isMatched: true
                }
                currentEvent.setAttribute("data-grid",JSON.stringify(attribute));

                score += 10;
                document.getElementById("score-value").innerText = score;

                matchNumber += 1;
                if(matchNumber === (index/2)) {
                    setTimeout(() => {
                        document.getElementsByClassName("game-wrapper")[0].style.display = "none";
                        document.getElementsByClassName("win-stats-wrapper")[0].style.display = "flex";
                        document.getElementById("final-moves").innerText = moves;

                        if(index === 4 && time < (10 * 1000)) {
                            if(moves <= 7) {
                                score += 10;
                            }
                            score += 10;
                        } else if(index === 8 && time < (20 * 1000)) {
                            if(moves <= 12) {
                                score += 10;
                            }
                            score += 10;
                        } else if(index === 12 && time < (30 * 1000)) {
                            if(moves <= 18) {
                                score += 10;
                            }
                            score += 10;
                        } else if(index === 16 && time < (100 * 1000)){
                            if(moves <= 22) {
                                score += 10;
                            }
                            score += 10;
                        } 
                        document.getElementById("final-score").innerText = score;

                        if(highestScoreLevel1 !== null && index === 4) {
                            highestScoreLevel1 = parseInt(highestScoreLevel1);
                            if(score > highestScoreLevel1) {
                                localStorage.setItem("highestScoreLevel1",score);
                                document.getElementById("highest").innerText = score;
                                console.log(document.getElementById("congrats-icon"));
                                document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                                document.getElementById("win-msg").innerText = "new high score";
                            } else {
                                document.getElementById("highest").innerText = highestScoreLevel1;
                            }
                        } else if(index === 4){
                            localStorage.setItem("highestScoreLevel1",score);
                            document.getElementById("highest").innerText = score;
                            document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                            document.getElementById("win-msg").innerText = "new high score";
                        }

                        if(highestScoreLevel2 !== null && index === 8) {
                            highestScoreLevel2 = parseInt(highestScoreLevel2);
                            if(score > highestScoreLevel2) {
                                localStorage.setItem("highestScoreLevel2",score);
                                document.getElementById("highest").innerText = score;
                                document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                                document.getElementById("win-msg").innerText = "new high score";
                            } else {
                                document.getElementById("highest").innerText = highestScoreLevel2;
                            }
                        } else if(index === 8){
                            localStorage.setItem("highestScoreLevel2",score);
                            document.getElementById("highest").innerText = score;
                            document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                            document.getElementById("win-msg").innerText = "new high score";
                        }

                        if(highestScoreLevel3 !== null && index === 12) {
                            highestScoreLevel3 = parseInt(highestScoreLevel3);
                            if(score > highestScoreLevel3) {
                                localStorage.setItem("highestScoreLevel3",score);
                                document.getElementById("highest").innerText = score;
                                document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                                document.getElementById("win-msg").innerText = "new high score";
                            } else {
                                document.getElementById("highest").innerText = highestScoreLevel3;
                            }
                        } else if(index === 12){
                            localStorage.setItem("highestScoreLevel3",score);
                            document.getElementById("highest").innerText = score;
                            document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                            document.getElementById("win-msg").innerText = "new high score";
                        }

                        if(highestScoreLevel4 !== null && index === 16) {
                            highestScoreLevel4 = parseInt(highestScoreLevel4);
                            if(score > highestScoreLevel4) {
                                localStorage.setItem("highestScoreLevel4",score);
                                document.getElementById("highest").innerText = score;
                                document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                                document.getElementById("win-msg").innerText = "new high score";
                            } else {
                                document.getElementById("highest").innerText = highestScoreLevel4;
                            }
                        } else if(index === 16){
                            localStorage.setItem("highestScoreLevel4",score);
                            document.getElementById("highest").innerText = score;
                            document.getElementById("congrats-icon").src = "./assets/newWinner.jpg";
                            document.getElementById("win-msg").innerText = "new high score";
                        }

                        let level;
                        if(index == 4){
                            level = 1;
                        } else if(index == 8) {
                            level = 2;
                        } else if(index == 12) {
                            level = 3;
                        } else if(index == 16) {
                            level = 4;
                        }
                        sendGame(level,score,moves);
                        
                    }, 500);
                }

                lastIcon = "";
                lastGrid = "";
                lastGridEvent = "";
                count = 0;
            } else if(count === 2) {
                setTimeout(() => {
                    currentEvent.classList.remove("grid-visible");
                    currentEvent.classList.add("grid");
                    currentEvent.style.pointerEvents = "auto";
                    currentEvent.style.transform = "rotateY(0deg)";
                    lastGridEvent.classList.remove("grid-visible");
                    lastGridEvent.classList.add("grid");
                    lastGridEvent.style.pointerEvents = "auto";
                    lastGridEvent.style.transform = "rotateY(0deg)";
                    lastIcon = "";
                    lastGrid = "";
                    lastGridEvent = "";
                    count = 0;
                },1000);
            } else {
                lastIcon = currentIcon;
                lastGrid = currentGrid;
                lastGridEvent = currentEvent;
            }
        }
    }

    let grids = document.getElementById("grids");
    for(let gridId=0;gridId<index;gridId++) {
        let grid  = document.createElement("div");
        grid.classList.add("grid");
        attribute = {
            id: gridId,
            isMatched: false
        };
        grid.setAttribute("data-grid", JSON.stringify(attribute));
        grid.addEventListener('click', listener);
        grids.append(grid);
    }

    let modal = document.getElementById("restart-modal");
    document.getElementById("restart").addEventListener("click",() => {
        modal.style.display = "flex";
    });

    let homeButtonStatus = false;
    document.getElementById("go-home-btn").addEventListener("click", () => {
        modal.style.display = "flex";
        homeButtonStatus = true;
    })

    document.getElementById("yes").addEventListener("click",() => {
        modal.style.display = "none";
        if(homeButtonStatus) {
            homeButtonStatus = false;
            location.reload();
        } else {
            document.getElementById("grids").innerHTML = "";
            document.getElementById("moves-value").innerHTML = "0";
            document.getElementById("score-value").innerHTML = "0";
            clearInterval(this.refreshInterval);
            game(index);
        }
    });

    document.getElementById("no").addEventListener("click",() => {
        modal.style.display = "none";
    });

    document.getElementById("play-again-btn").addEventListener("click", () => {
        location.reload();
    });;
}

window.onload = () => {
    preloadIcon("./assets/winner.jpg");
    preloadIcon("./assets/newWinner.jpg");
    preloadIconsArray(this.iconsUrl);
    localStorage.clear();
    document.getElementById("4-grid").addEventListener("click",() => {
        index = 4;
        game(index);
        document.getElementsByClassName("home-body")[0].style.display = "none";
    });
    document.getElementById("8-grid").addEventListener("click",() => {
        index = 8;
        game(index);
        document.getElementsByClassName("home-body")[0].style.display = "none";
    });
    document.getElementById("12-grid").addEventListener("click",() => {
        index = 12;
        game(index);
        document.getElementsByClassName("home-body")[0].style.display = "none";
    });
    document.getElementById("16-grid").addEventListener("click",() => {
        index = 16;
        game(index);
        document.getElementsByClassName("home-body")[0].style.display = "none";
    });

    let loginForm = document.getElementById("login-form");

    loginForm.addEventListener("submit", (e) => {
        e.preventDefault();
        let email = document.getElementById("email").value;
        let password= document.getElementById("password").value;
        
        fetch("http://localhost:4000/user/login",{
            method:'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        })
        .then((resp) => {
            return resp.json();  
        })
        .then((data) => {
            if(data.accessToken !== undefined) {
                localStorage.setItem("accessToken",data.accessToken);
                localStorage.setItem("refreshToken",data.refreshToken);
                getDashboard();
            } else {
                throw new Error("Incorrect Password or Email");
            }
        })
        .catch((err) => {
            console.log(err);
        });
    });
}