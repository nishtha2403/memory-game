window.onload = () => {
    let form = document.getElementById("form");
    
    form.addEventListener("submit", (e) => {
        e.preventDefault();
        let name = document.getElementById("name").value;
        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;
        fetch("http://localhost:4000/user/register", { 
            method:'POST', 
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                email,
                password
            })
        })
        .then((resp) => {
            return resp.json();
        })
        .then((data) => {
            console.log(data);
        })
        .catch((err) => {
            console.error(err);
        });
    });
}